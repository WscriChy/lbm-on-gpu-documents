#! python 
# encoding: utf-8

top = '.'
out = 'build'

def options(opt):
	pass

def configure(conf):
	conf.load('tex')
	if not conf.env.LATEX:
		conf.fatal('The program LaTex is required')

def build(bld):
    for x in [ 'Presentation' ]:
        bld(
            features = 'tex',
            type     = 'xelatex',                               # pdflatex or xelatex
            source   = 'source/%s.tex' % x, # mandatory, the source
            outs     = 'pdf',                           # 'pdf' or 'ps pdf'
        )
        bld.install_as(bld.path.abspath(), 'source/%s.pdf' % (x))
